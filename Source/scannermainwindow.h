#ifndef SCANNERMAINWINDOW_H
#define SCANNERMAINWINDOW_H

#include <QMainWindow>

class ScannerMainWindow : public QMainWindow
{
    Q_OBJECT

public:
    ScannerMainWindow(QWidget *parent = 0);
    ~ScannerMainWindow();
};

#endif // SCANNERMAINWINDOW_H
