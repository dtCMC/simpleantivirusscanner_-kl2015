#-------------------------------------------------
#
# Project created by QtCreator 2015-04-05T20:59:10
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SAS
TEMPLATE = app


SOURCES += main.cpp\
        scannermainwindow.cpp

HEADERS  += scannermainwindow.h
